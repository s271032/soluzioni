import sys


#\function to convert an array of char into a string
def convert(s):
    # initialization of string to ""
    str1 = ""

    # using join function join the list s by
    # separating words by str1
    return (str1.join(s))
# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath,'r')
    lines= file.readline()
    file.close()

    return True, lines

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    n=len(password)
    m=len(text)
   # text=text.upper()
    #print(text)
    encypted=[]
    for i in range(0,m-1) :
        x=ord(text[i])
        y=ord(password[(i%n)])
        encypted.append(chr((x+y-64)))
    cifrato=convert(encypted)
    #print(encypted)
    #print(cifrato)
    return True, cifrato

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    n = len(password)
    m = len(text)
    decypted = []
    for i in range(0,m) :
        x=ord(text[i])
        y=ord(password[(i%n)])
        decypted.append(chr(x-y+64))
    #print(decypted)
    decifrato=convert(decypted)
    # print(decifrato)

    return True, decifrato+"\n"


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
